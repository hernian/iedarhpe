""" 動画の各フレームの骨格を推論し画像にする
"""
import argparse
import glob
import os
import cv2
from humanposeestimator import HumanPoseEstimator


FNAME_MODEL = 'model/human-pose-estimation-0001.xml'
FNAME_WEIGHTS = 'model/human-pose-estimation-0001.bin'
NAME_DEVICE = 'MYRIAD'


def main() -> None:
    hpe = HumanPoseEstimator(FNAME_MODEL, FNAME_WEIGHTS, NAME_DEVICE)
    fnames = glob.glob('navi_left_place_1s_01/0001.png')
    fnames.sort()
    for fname_in in fnames:
        fname_dir_in = os.path.dirname(fname_in)
        fname_dir_out = f'{fname_dir_in}_hpe'
        os.makedirs(fname_dir_out, exist_ok=True)
        fname_base = os.path.basename(fname_in)
        fname_out = os.path.join(fname_dir_out, fname_base)
        image_in = cv2.imread(fname_in)
        pks, image_res = hpe.estimate(image_in)
        cv2.imwrite(fname_out, image_res)
        print(fname_out)


if __name__ == "__main__":
    main()
