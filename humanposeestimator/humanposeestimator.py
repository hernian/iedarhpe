""" 人体骨格推論
"""
import cv2
import numpy as np
import time
from openvino.inference_engine import IECore


_TAG = '[HumanPoseEstimator]'
BLOB_NAME_INPUT = 'data'
BLOB_NAME_PCM = 'Mconv7_stage2_L2'
BLOB_NAME_PAF = 'Mconv7_stage2_L1'
COUNT_KEYPOINT = 18
OFFSET_PAFS = 19
RADIUS_KEYPOINT = 2
LINE_WIDTH_BONE = 2
THRESHOLD_KEYPOINTS = 0.1


POSE_PAIRS = [[1,2], [1,5], [2,3], [3,4], [5,6], [6,7],
              [1,8], [8,9], [9,10], [1,11], [11,12], [12,13],
              [1,0], [0,14], [14,16], [0,15], [15,17],
              [2,17], [5,16] ]

MAP_IDXS = [[31,32], [39,40], [33,34], [35,36], [41,42], [43,44],
            [19,20], [21,22], [23,24], [25,26], [27,28], [29,30],
            [47,48], [49,50], [53,54], [51,52], [55,56],
            [37,38], [45,46]]

COLORS_KEYPOINT = [[0,100,255], [0,100,255], [0,255,255], [0,100,255], [0,255,255], [0,100,255],
                   [0,255,0], [255,200,100], [255,0,255], [0,255,0], [255,200,100], [255,0,255],
                   [0,0,255], [255,0,0], [200,200,0], [255,0,0], [200,200,0], [0,0,0]]

class HumanPoseEstimator(object):
    def __init__(self, fname_model: str, fname_weights: str, name_device: str) -> None:
        self.ie = IECore()
        self.net = self.ie.read_network(model=fname_model, weights=fname_weights)
        self.exec_net = self.ie.load_network(
            network=self.net,
            device_name=name_device,
            config={},
            num_requests=1)
        blob_input = self.net.input_info[BLOB_NAME_INPUT]
        w = blob_input.input_data.shape[3]
        h = blob_input.input_data.shape[2]
        self.size_model_in = (w, h)
        self.shape_image_in = (h, w, 3)
        print('shape_image_in:{0}'.format(self.shape_image_in))


    def _get_input_blob(self, image_in : np.ndarray) -> np.ndarray:
        w_image_in = image_in.shape[1]
        h_image_in = image_in.shape[0]
        image_in_scaled = cv2.resize(image_in, self.size_model_in, interpolation=cv2.INTER_CUBIC)

#        print('w_image_in:{0}, h_image_in:{1}'.format(w_image_in, h_image_in))
        image_temp = np.full(self.shape_image_in, 128)
        image_temp[0:h_image_in, 0:w_image_in, :] = image_in_scaled
        blob_temp = image_temp[np.newaxis, :, :, :]
        blob_in = blob_temp.transpose((0, 3, 1, 2))
        return blob_in


    def _detect_keypoints(self, blob_pcm):
        detected_keypoints = []
        keypoints_list = np.zeros((0, 3))
        keypoint_id = 0
        for part in range(COUNT_KEYPOINT):
            map_pcm = blob_pcm[0, part, :, :]
            keypoints = self._get_keypoints(map_pcm, THRESHOLD_KEYPOINTS)
            keypoints_with_id = []
            for i, keypoint in enumerate(keypoints):
                keypoints_with_id.append(keypoint + (keypoint_id,))
                keypoints_list = np.vstack([keypoints_list, keypoint])
                keypoint_id += 1
            detected_keypoints.append(keypoints_with_id)
        return detected_keypoints, keypoints_list


    def _get_keypoints(self, map_pcm, threshold=0.1):
        map_pcm_in_size = cv2.resize(map_pcm, self.size_model_in)
        map_pcm_smooth = cv2.GaussianBlur(map_pcm_in_size, (3, 3), 0, 0)
        map_mask = np.uint8(map_pcm_smooth>threshold)
        keypoints = []
        contours = None
        try:
            # OpenCV4.x
            contours, _ = cv2.findContours(map_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        except:
            # OpenCV3.x
            _, contours, _ = cv2.findContours(map_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        for cnt in contours:
            blob_mask = np.zeros(map_mask.shape)
            blob_mask = cv2.fillConvexPoly(blob_mask, cnt, 1)
            map_masked_prob = map_pcm_smooth * blob_mask
            _, max_val, _, max_loc = cv2.minMaxLoc(map_masked_prob)
            keypoints.append(max_loc + (map_pcm_in_size[max_loc[1], max_loc[0]],))
        return keypoints


    def _get_vallid_pairs(self, blob_paf, detected_keypoints):
        valid_pairs = []
        invalid_pairs = []
        n_interp_samples = 10
        paf_score_th = 0.1
        conf_th = 0.7

        for k in range(len(MAP_IDXS)):
            map_idx = (MAP_IDXS[k][0] - OFFSET_PAFS, MAP_IDXS[k][1] - OFFSET_PAFS)
            pose_pair = POSE_PAIRS[k]
            paf_a = blob_paf[0, map_idx[0], :, :]
            paf_b = blob_paf[0, map_idx[1], :, :]
            paf_a_in_size = cv2.resize(paf_a, self.size_model_in)
            paf_b_in_size = cv2.resize(paf_b, self.size_model_in)
            cand_a = detected_keypoints[pose_pair[0]]
            cand_b = detected_keypoints[pose_pair[1]]
            n_a = len(cand_a)
            n_b = len(cand_b)
            if n_a != 0 and n_b != 0:
                valid_pair = np.zeros((0, 3))
                for ca in cand_a:
                    cb_max = None
                    max_score = -1
                    for cb in cand_b:
                        d_ij = np.subtract(cb[:2], ca[:2])
                        norm = np.linalg.norm(d_ij)
                        if norm == 0:
                            continue
                        d_ij = d_ij / norm
                        interp_coord = list(zip(np.linspace(ca[0], cb[0], num=n_interp_samples),
                                                np.linspace(ca[1], cb[1], num=n_interp_samples)))
                        paf_interp = []
                        for ic in interp_coord:
                            paf_interp.append([paf_a_in_size[int(round(ic[1])), int(round(ic[0]))], \
                                               paf_b_in_size[int(round(ic[1])), int(round(ic[0]))] ])
                        paf_scores = np.dot(paf_interp, d_ij)
                        avg_paf_score = sum(paf_scores) / len(paf_scores)
                        if (len(np.where(paf_scores>paf_score_th)[0]) / n_interp_samples) > conf_th:
                            if avg_paf_score > max_score:
                                cb_max = cb
                                max_score = avg_paf_score
                    if cb_max is not None:
                        valid_pair = np.append(valid_pair, [[ca[3], cb_max[3], max_score]], axis=0)
                valid_pairs.append(valid_pair)
            else:
                invalid_pairs.append(k)
                valid_pairs.append([])
        return valid_pairs, invalid_pairs


    def _get_parsonwise_keypoints(self, keypoints_list, valid_pairs, invalid_pairs):
        parsonwise_keypoints = -1 * np.ones((0, 19))
        for k in range(len(MAP_IDXS)):
            if k not in invalid_pairs:
                part_as = valid_pairs[k][:,0]
                part_bs = valid_pairs[k][:,1]
                index_a, index_b = np.array(POSE_PAIRS[k])
                for i, valid_pair in enumerate(valid_pairs[k]):
                    found = False
                    parson_idx = -1
                    for j, parsonwise_keypoint in enumerate(parsonwise_keypoints):
                        if parsonwise_keypoint[index_a] == part_as[i]:
                            parson_idx = j
                            found = True
                            break
                    if found:
                        parsonwise_keypoints[parson_idx][index_b] = part_bs[i]
                        parsonwise_keypoints[parson_idx][-1] += keypoints_list[part_bs[i].astype(int), 2] + \
                                                                valid_pairs[k][i][2]
                    elif k < 17:
                        row = -1 * np.ones(19)
                        row[index_a] = part_as[i]
                        row[index_b] = part_bs[i]
                        row[-1] = sum(keypoints_list[valid_pairs[k][i,:2].astype(int), 2]) + \
                                  valid_pairs[k][i][2]
                        parsonwise_keypoints = np.vstack([parsonwise_keypoints, row])
        return parsonwise_keypoints


    def _create_result(self, parsonwise_keypoints, keypoints_list) -> np.ndarray:
        count_keypoint = len(parsonwise_keypoints)
        count_edge = COUNT_KEYPOINT - 1
        result = np.zeros((count_keypoint, count_edge, 2, 2), dtype=np.float32)
        for i0, pwk in enumerate(parsonwise_keypoints):
            for i1 in range(COUNT_KEYPOINT - 1):
                index = pwk[np.array(POSE_PAIRS[i1])].astype(int)
                if -1 in index:
                    x1 = -1.0
                    y1 = -1.0
                    x2 = -1.0
                    y2 = -1.0
                else:
                    b = keypoints_list[index, 0]
                    a = keypoints_list[index, 1]
                    x1 = b[0]
                    y1 = a[0]
                    x2 = b[1]
                    y2 = a[1]
                result[i0, i1, 0, 0] = x1
                result[i0, i1, 0, 1] = y1
                result[i0, i1, 1, 0] = x2
                result[i0, i1, 1, 1] = y2
        return result


    def _render_pose(self, parsonwise_keypoints, keypoints_list, image) -> None:
        for pwk in parsonwise_keypoints:
            for i in range(COUNT_KEYPOINT - 1):
                index = pwk[np.array(POSE_PAIRS[i])].astype(int)
                if -1 in index:
                    continue
                b = keypoints_list[index, 0]
                a = keypoints_list[index, 1]
                x1 = int(b[0])
                y1 = int(a[0])
                x2 = int(b[1])
                y2 = int(a[1])
                color = COLORS_KEYPOINT[i]
                cv2.line(image, (x1, y1), (x2, y2), color, LINE_WIDTH_BONE, cv2.LINE_AA)


    def _render_pose2(self, result, image) -> None:
        x_scale = image.shape[1] / self.shape_image_in[1]
        y_scale = image.shape[0] / self.shape_image_in[0]
        for idx_parson in range(result.shape[0]):
            for idx_bone in range(result.shape[1]):
                x1 = int(result[idx_parson, idx_bone, 0, 0] * x_scale)
                y1 = int(result[idx_parson, idx_bone, 0, 1] * y_scale)
                x2 = int(result[idx_parson, idx_bone, 1, 0] * x_scale)
                y2 = int(result[idx_parson, idx_bone, 1, 1] * y_scale)
                color = COLORS_KEYPOINT[idx_bone]
                cv2.line(image, (x1, y1), (x2, y2), color, LINE_WIDTH_BONE, cv2.LINE_AA)


    def estimate(self, image_in: np.ndarray) -> np.ndarray:
        t1 = time.time()
        blob_in = self._get_input_blob(image_in)
        req_id = 0
        self.exec_net.start_async(request_id=req_id, inputs={BLOB_NAME_INPUT: blob_in})
        req = self.exec_net.requests[req_id]
        r = req.wait(-1)
        if r != 0:
            raise BrError('{0}error'.format(_TAG))
        t2 = time.time()
        blob_pcm = req.output_blobs[BLOB_NAME_PCM]
        blob_paf = req.output_blobs[BLOB_NAME_PAF]
        detected_keypoints, keypoints_list = self._detect_keypoints(blob_pcm.buffer)
        valid_pairs, invalid_pairs = self._get_vallid_pairs(blob_paf.buffer,
                                                            detected_keypoints)
        parsonwise_keypoints = self._get_parsonwise_keypoints(keypoints_list, valid_pairs, invalid_pairs)
        print('count parson:{0}'.format(len(parsonwise_keypoints)))
        result = self._create_result(parsonwise_keypoints, keypoints_list)
        t3 = time.time()
        image = image_in.copy()
        self._render_pose2(result, image)
        print('{0}inference time:{1:.3f}, postprocess time:{2:.3f}'.format(_TAG, t2 - t1, t3 - t2))
        return result, image
